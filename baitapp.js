// bai 1:
// Input: so ngay lam viec

// Step:
// S1: tao bien cho so ngay lam
// S2: tao bien so tien luong ((voi tien luong 1 ngay =1000000)
// S3: in ket qua ra consolog (tien luong cua nhan vien)

// Output: 30000000
var soNgay = 30;
var tienLuong = null;
tienLuong = 1000000 * soNgay;
console.log('tienLuong: ', tienLuong);


// Bai 2
// Input: 5 so thuc

// Step:
// S1: tao bien cho 5 so thuc
// S2: tao bien gia tri trung binh ((voi gia tri trung binh la tong 5 so / 5))
// S3: in ket qua ra consolog 

// Output: 25

var soThuc1 = 5;
var soThuc2 = 15;
var soThuc3 = 25;
var soThuc4 = 35;
var soThuc5 = 45;
var trungBinh = null;
trungBinh = (soThuc1 + soThuc2 + soThuc3 + soThuc4 + soThuc5) / 5;
console.log('trungBinh: ', trungBinh);


// Bai 3
// Input: so tien USD

// Step:
// S1: tao bien cho so tien USD & VND
// S2: tao bien ket qua 
// S3: in ket qua ra consolog 

// Output: 1175000

var tienUsd = 50;
var tienVnd = null;
tienVnd = 23500 * tienUsd;
console.log('tienVnd: ', tienVnd);


// Bai 4
// Input: chieu dai $ chieu rong HCN

// Step:
// S1: tao bien cho chieu dai & chieu rong
// S2: tao bien cho dien tich & chu vi (voi cong thuc tinh dien tich = dai*rong ; chu vi=(dai+rong)/2
// S3: in ket qua ra consolog 

// Output: dien tich = 200 ; chuvi = 15

var chieuDai = 10;
var chieuRong = 20;
var dienTich = null;
var chuVi = null;
dienTich = chieuDai * chieuRong;
console.log('dienTich: ', dienTich);
chuVi = (chieuDai + chieuRong) / 2;
console.log('chuVi: ', chuVi);

// Bai 5
// Input: so co 2 chu so (88)

// Step:
// S1: tao bien cho so 
// S2: tao bien cho so hang chuc & don vi
// S3: in ket qua ra consolog 

// Output: 16

var number = 88;
var donVi = number % 10;
var hangChuc = Math.floor(number / 10) % 10;
var tong = donVi + hangChuc;
console.log('tong: ', tong);



